import random, string

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


def generate_random_string(length):
    """Generate random string
                :param length: number of characters
                :return: characters concatenated into string
    """
    chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
    return ''.join(random.choice(chars) for _ in range(length))


def wait_for_element_not_visible(driver, timeout, *locator):
    condition = EC.visibility_of_element_located(locator)
    wait = WebDriverWait(driver, timeout)
    return wait.until_not(condition)
