from selenium.webdriver.common.by import By
from library.helpers import generate_random_string
from pages.base_page import BasePage


class UserRolePage(BasePage):
    new_user_role_type = "ESS"
    new_user_role_name = "AUTO" + generate_random_string(5)
    new_user_role_name_edit = "EDIT"

    type_input_locator = (By.CSS_SELECTOR, "div[id='data-group-user-selection-select'] input")
    user_role_name_input_initiate_locator = (By.CSS_SELECTOR, "label[for='user_role_name']")
    user_role_name_input_locator = (By.ID, "user_role_name")
    user_role_name_required_validation_message_locator = (
        By.CSS_SELECTOR, "span[ng-show='userRoleForm.userRoleName.$error.required']:not([class~='ng-hide'])")
    save_button_locator = (By.CSS_SELECTOR, "a[class~='primary-btn']")

    def get_type_option_locator(self, user_role_type):
        return (By.XPATH, f"//div[@id='data-group-user-selection-select']//span[text()='{user_role_type}']")

    def click_save_button(self):
        element = self.driver.find_element(*self.save_button_locator)
        element.click()

    def is_user_role_name_required_validation_message_displayed(self):
        element = self.driver.find_element(*self.user_role_name_required_validation_message_locator)
        return element.is_displayed()

    def input_user_role_type(self, user_role_type):
        element = self.driver.find_element(*self.type_input_locator)
        element.click()
        element = self.driver.find_element(*self.get_type_option_locator(user_role_type))
        element.click()
        self.wait_for_preloader_not_visible()

    def input_user_role_name(self, user_role_name):
        element = self.driver.find_element(*self.user_role_name_input_initiate_locator)
        element.click()
        element = self.driver.find_element(*self.user_role_name_input_locator)
        element.send_keys(user_role_name)

    def fill_add_user_role_data(self, user_role_type, user_role_name):
        self.input_user_role_type(user_role_type)
        self.input_user_role_name(user_role_name)

    def edit_user_role_name(self, user_role_name_edit):
        self.wait_for_preloader_not_visible()
        element = self.driver.find_element(*self.user_role_name_input_locator)
        element.click()
        element.send_keys(user_role_name_edit)
