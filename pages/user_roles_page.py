from selenium.webdriver.common.by import By
from configuration import config_loader
from pages.base_page import BasePage


class UserRolesPage(BasePage):
    configuration = config_loader.load_configuration()
    implicitly_timeout = configuration["Implicitly timeout"]
    environment = config_loader.load_environment()
    base_url = environment["Base URL"]
    url = base_url + environment["User Roles Page URL path"]

    add_user_role_button_locator = (By.CSS_SELECTOR, "div[class~='floating-add-btn']")
    list_container_locator = (By.CSS_SELECTOR, ".list-container")
    user_role_type_locator = (By.XPATH, "//td[3]//span")
    user_role_edit_locator = (By.XPATH, "//td[contains(@class,'edit_item')]")
    user_role_checkbox_locator = (By.XPATH, "//input[@type='checkbox']/following-sibling::label")
    table_options_button_locator = (By.CSS_SELECTOR, "th[class='list-options'] a i")
    table_option_delete_selected_locator = (By.XPATH, "//a[text()='Delete Selected']")
    delete_confirm_button_locator = (
        By.CSS_SELECTOR, "div[id='delete_confirmation_modal'] a[ng-click='modal.confirm()']")
    success_message_locator = (By.CSS_SELECTOR, "div[class~='toast-success']")

    def get_user_role_row_locator(self, user_role_name):
        return (By.XPATH, f"//*/tbody//td[2]//span[text()='{user_role_name}']/../../..")

    def click_add_user_role_button(self):
        element = self.driver.find_element(*self.add_user_role_button_locator)
        element.click()
        self.wait_for_preloader_not_visible()

    def click_user_role_edit(self, user_role_name):
        list_container_element = self.driver.find_element(*self.list_container_locator)
        user_role_row_element = list_container_element.find_element(*self.get_user_role_row_locator(user_role_name))
        user_role_type_element = user_role_row_element.find_element(*self.user_role_edit_locator)
        user_role_type_element.click()

    def click_user_role_checkbox(self, user_role_name):
        list_container_element = self.driver.find_element(*self.list_container_locator)
        user_role_row_element = list_container_element.find_element(*self.get_user_role_row_locator(user_role_name))
        user_role_checkbox_element = user_role_row_element.find_element(*self.user_role_checkbox_locator)
        user_role_checkbox_element.click()

    def choose_table_option_delete_selected(self):
        table_options_button_element = self.driver.find_element(*self.table_options_button_locator)
        table_options_button_element.click()
        table_option_delete_selected_element = table_options_button_element.find_element(
            *self.table_option_delete_selected_locator)
        table_option_delete_selected_element.click()
        return self

    def confirm_table_option_delete_selected(self):
        self.driver.find_element(*self.delete_confirm_button_locator).click()

    def check_table_row_present(self, user_role_name, user_role_type):
        user_role_row_locator = self.get_user_role_row_locator(user_role_name)
        self.driver.find_element(*self.success_message_locator)
        self.load()
        self.driver.find_element(*self.list_container_locator)
        self.driver.implicitly_wait(0)
        user_role_row_element_list = self.driver.find_elements(*user_role_row_locator)
        self.driver.implicitly_wait(self.implicitly_timeout)
        if len(user_role_row_element_list) == 0:
            return False
        else:
            assert user_role_row_element_list[0].find_element(*self.user_role_type_locator).text == user_role_type
            return True
