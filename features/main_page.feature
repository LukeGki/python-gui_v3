@functional_tests
Feature: Main page
  Main page tests

  Background:
    Given I am logged in

  Scenario: Log out
    When I log out
    Then I should see correct page url

  Scenario Outline: My Shortcuts menu
    When I choose My Shortcuts menu option <option>
    Then I should see correct page url
    Examples:
      | option       |
      | Assign Leave |
      | Leave List   |

  Scenario Outline: Admin menu
    When I choose Admin - User Management menu option <option>
    Then I should see correct page url
    Examples:
      | option     |
      | User Roles |
      | Users      |