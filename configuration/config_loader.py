import json
import os


def load_configuration():
    return json.loads(open(os.path.join(os.path.dirname(__file__), "config_data.json"), "r").read())


def load_environment():
    configuration = load_configuration()
    environment = configuration["Environment"]
    return json.loads(open(os.path.join(os.path.dirname(__file__), "env_" + environment + ".json"), "r").read())
